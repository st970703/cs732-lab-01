import { useState } from 'react';
import ToDoList from './ToDoList';
import NewTodoItem from './NewTodoItem';


// A to-do list to use for testing purposes
const initialTodos = [
  { description: 'Finish lecture', isComplete: true },
  { description: 'Do homework', isComplete: false },
  { description: 'Sleep', isComplete: true }
];



function App() {

  const [todos, setTodos] = useState(initialTodos);

  function handleTodoChange(index, isComplete) {
    const newTodos = [...todos];
    newTodos[index] = { ...todos[index], isComplete };

    setTodos(newTodos);
  }

  function handleAddTodo(description) {
    const newTodos = [...todos];
    newTodos.push({ description, isComplete: false });

    setTodos(newTodos);
  }

  function handleRemoveTodo(index) {
    const newTodos = [...todos];
    newTodos.splice(index, 1);

    setTodos(newTodos);
  }

  return (
    <div>
      <div>
        <h1 className="align-header">My todos</h1>
        <ToDoList items={todos}
          onTodoStatusChanged={handleTodoChange}
          onRemoveTodo={handleRemoveTodo} />
      </div>
      <div className="addItemContainer">
        <h1>Add item</h1>
        <NewTodoItem onAddTodo={handleAddTodo} />
      </div>
    </div>
  );
}

export default App;