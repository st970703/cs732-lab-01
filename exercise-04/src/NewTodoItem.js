import { useState } from 'react';
import styles from './NewTodoItem.module.css';

export default function NewTodoItem({ onAddTodo }) {

    const [description, setDescription] = useState('');

    return (
        <div style={{position: "relative"}}>
            <label  className={styles.align_input}
            >Description:</label>
            <input type="text" value={description}
                onInput={e => setDescription(e.target.value)} />
            <button onClick={() => onAddTodo(description)}
            class="align_add_btn_right"
            >Add</button>
        </div>
    );
}