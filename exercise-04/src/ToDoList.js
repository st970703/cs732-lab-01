import styles from './ToDoList.module.css';

export default function ToDoList({ items, onTodoStatusChanged, onRemoveTodo }) {
    if (items && items.length > 0) {
        return (
            items.map((item, index) => (
                <div key={index}
                    className={item.isComplete ? styles.todoItemContainer_complete : styles.todoItemContainer_incomplete}>
                    <input type="checkbox"
                        checked={item.isComplete}
                        className={styles.align_checkbox}
                        onChange={e => onTodoStatusChanged(index, e.target.checked)} />
                    <label>
                        {item.description}
                        {item.isComplete &&
                            <span> (Done!)</span>}
                    </label>
                    <button onClick={e => onRemoveTodo(index)}
                    class="align_add_btn_right"
                    >Remove</button>
                </div>
            ))
        );
    } else {
        return (
            <p>There are no to-do items!</p>
        );
    }
}